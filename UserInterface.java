package br.edu.ifce.space;
import net.jini.core.entry.Entry;
import net.jini.core.entry.UnusableEntryException;
import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.EventListener;

import static net.jini.core.lease.Lease.FOREVER;

public class UserInterface {

    private ArrayList<Environment> add_again;
    public boolean always;

    public void ListEnvironment(JavaSpace space) throws TransactionException, UnusableEntryException,
            RemoteException, InterruptedException {

        add_again = new ArrayList<>();
        always = true;
        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }
        while (always) {

            Environment template = new Environment();
            Environment obj = (Environment) space.take(template, null, 100);
            add_again.add(obj);

            if (obj == null) {
                for (Environment ob : add_again) {
                    if (ob == null){
                        always = false;
                    }else {
                        space.write(ob,null, FOREVER);
                    }
                }

            }else {
                System.out.println(obj.name);
            }

        }
    }

    public void AddUser(JavaSpace space, Environment env, String user_name){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {

            Environment obj = (Environment) space.take(env, null, 100);

            if (obj == null){
                System.out.println("Ambiente não encontrado!");
                System.exit(0);

            }else {
                if (obj.users != null){
                    for (int i=0; i < obj.users.size(); i++){
                        if (obj.users.get(i).equals(user_name)){
                            obj.users.remove(user_name);
                            System.out.println("Usuário já existe");
                        }
                    }
                    obj.users.add(user_name);
                    space.write(obj, null, FOREVER);

                }else {
                    System.out.println("Você é o primeiro usuário deste ambiente");
                    obj.users = new ArrayList<String>();
                    obj.users.add(user_name);
                    space.write(obj, null, FOREVER);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void MoveUser(JavaSpace space, Environment env1, String user_name, Environment env2){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {

            Environment obj1 = (Environment) space.take(env1, null, 100);
            Environment obj2 = (Environment) space.take(env2, null, 100);

            if (obj1 == null){
                System.out.println("Ambiente atual não encontrado!");

            }else {
                if (obj1.users != null){
                    for (int i=0; i < obj1.users.size(); i++){
                        if (obj1.users.get(i).equals(user_name)){
                            obj1.users.remove(user_name);
                        }
                    }
                    space.write(obj1, null, FOREVER);

                }else {
                    System.out.println("Não há usuários para mover");
                }
            }

            if (obj2 != null){
                if (obj2.users != null){
                    for (int i=0; i < obj2.users.size(); i++){
                        if (obj2.users.get(i).equals(user_name)){
                            obj2.users.remove(user_name);
                        }
                    }
                    obj2.users.add(user_name);
                    space.write(obj2, null, FOREVER);
                    System.out.println("usuário movido!");

                }else {
                    obj2.users = new ArrayList<String>();
                    obj2.users.add(user_name);
                    space.write(obj2, null, FOREVER);
                    System.out.println("usuário movido!");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void ListUsers(JavaSpace space, Environment env){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {
            Environment obj = (Environment) space.take(env, null, 100);

            if (obj == null){

                System.out.println("Ambiente não encontrado!");

            }else {
                if (obj.users != null){
                    for (int i=0; i < obj.users.size(); i++){
                        System.out.println(obj.users.get(i));
                    }
                } else {
                    System.out.println("Não há usuários neste ambiente!");
                }
                space.write(obj, null, FOREVER);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
