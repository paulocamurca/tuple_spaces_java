package br.edu.ifce.space;

import net.jini.space.JavaSpace;

import java.util.Scanner;

public class Manager {
    public static void main(String[] args) {
        try {
            System.out.println("Procurando pelo servico JavaSpace...");
            Lookup finder = new Lookup(JavaSpace.class);
            JavaSpace space = (JavaSpace) finder.getService();

            if (space == null) {
                System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
                System.exit(-1);
            }
            System.out.println("O servico JavaSpace foi encontrado.");

            Interface inter = new Interface();

            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.print("\033[H\033[2J");
                System.out.print("\n--------------------------------------------------\n");
                System.out.print("Escolha uma opcao do menu (ENTER para sair): \n");
                System.out.print("1 - criar ambiente\n");
                System.out.print("2 - listar ambientes\n");
                System.out.print("3 - remover ambiente\n");
                System.out.print("4 - adicionar dispositivos\n");
                System.out.print("5 - listar dispositivos\n");
                System.out.print("6 - mover dispositivos\n");
                System.out.print("7 - listar usuários\n");
                System.out.print("----------------------------------------------------\n");

                String keyboard = scanner.nextLine();
                if (keyboard == null || keyboard.equals("")) {
                    System.exit(0);
                }

                // create environment
                if (keyboard.equals("1")){
                    System.out.print("Entre com o nome do ambiente (ENTER para sair): ");
                    String env_name = scanner.nextLine();
                    Environment env = new Environment();
                    env.name = env_name;
                    inter.CreateEnvironment(space, env);
                }

                // list environments
                if (keyboard.equals("2")){
                    inter.ListEnvironment(space);
                }

                // remove empty environments
                if (keyboard.equals("3")){
                    System.out.print("Entre com o nome do ambiente a ser removido (ENTER para sair): ");
                    String env_name = scanner.nextLine();
                    Environment env = new Environment();
                    env.name = env_name;
                    inter.RemoveEnvironment(space, env);
                }

                // add device
                if (keyboard.equals("4")){
                    System.out.print("Entre com o nome do ambiente (ENTER para sair): ");
                    String env_name = scanner.nextLine();
                    System.out.print("Entre com o nome do dispositivo (ENTER para sair): ");
                    String device_name = scanner.nextLine();
                    Environment env = new Environment();
                    env.name = env_name;
                    inter.AddDevice(space, env, device_name);
                }

                // list devices
                if (keyboard.equals("5")){
                    System.out.print("Entre com o nome do ambiente para listar os dispositivos (ENTER para sair): ");
                    String env_name = scanner.nextLine();
                    Environment env = new Environment();
                    env.name = env_name;
                    inter.ListDevices(space, env);
                }

                // move devices
                if (keyboard.equals("6")){
                    System.out.print("Entre com o nome do ambiente (ENTER para sair): ");
                    String env_name = scanner.nextLine();
                    System.out.print("Entre com o nome do dispositivo (ENTER para sair): ");
                    String device_name = scanner.nextLine();
                    System.out.print("Entre com o nome do novo ambiente (ENTER para sair): ");
                    String new_env_name = scanner.nextLine();
                    Environment env_old = new Environment();
                    env_old.name = env_name;
                    Environment new_env = new Environment();
                    new_env.name = new_env_name;
                    inter.MoveDevice(space, env_old, device_name, new_env);
                }

                // list users
                if (keyboard.equals("7")){
                    System.out.print("Entre com o nome do ambiente para listar os usuários (ENTER para sair): ");
                    String env_name = scanner.nextLine();
                    Environment env = new Environment();
                    env.name = env_name;
                    inter.ListUsers(space, env);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
