package br.edu.ifce.space;

import net.jini.space.JavaSpace;

import java.util.Scanner;

public class User {
    public static void main(String[] args) {
        try {
            System.out.println("Procurando pelo servico JavaSpace...");
            Lookup finder = new Lookup(JavaSpace.class);
            JavaSpace space = (JavaSpace) finder.getService();

            if (space == null) {
                System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
                System.exit(-1);
            }
            System.out.println("O servico JavaSpace foi encontrado.");

            UserInterface inter = new UserInterface();

            inter.ListEnvironment(space);

            Scanner scanner = new Scanner(System.in);

            // add user
            System.out.print("Entre com o nome de usuário (ENTER para sair): ");
            String user_name = scanner.nextLine();

            Environment main_env = new Environment();

            if (user_name != null || !user_name.equals("")){
                System.out.print("Entre com o nome do ambiente (ENTER para sair): ");
                String main_name = scanner.nextLine();
                main_env.name = main_name;
                inter.AddUser(space, main_env, user_name);
            }else {
                System.exit(0);
            }

            while (true) {
                System.out.print("\033[H\033[2J");
                System.out.print("\n--------------------------------------------------\n");
                System.out.print("Escolha uma opcao do menu (ENTER para sair): \n");
                System.out.print("1 - listar ambientes\n");
                System.out.print("2 - listar usuários\n");
                System.out.print("3 - mudar de ambiente\n");
                System.out.print("----------------------------------------------------\n");

                String keyboard = scanner.nextLine();
                if (keyboard == null || keyboard.equals("")) {
                    System.exit(0);
                }

                // list environments
                if (keyboard.equals("1")){
                    inter.ListEnvironment(space);
                }

                // list users
                if (keyboard.equals("2")){
                    System.out.print("Entre com o nome do ambiente para listar os usuários (ENTER para sair): ");
                    String user_env_name = scanner.nextLine();
                    Environment env = new Environment();
                    env.name = user_env_name;
                    inter.ListUsers(space, env);
                }

                // move user
                if (keyboard.equals("3")){
                    System.out.print("Entre com o nome do novo ambiente (ENTER para sair): ");
                    String new_env_name = scanner.nextLine();
                    Environment new_env = new Environment();
                    new_env.name = new_env_name;
                    inter.MoveUser(space, main_env, user_name, new_env);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
