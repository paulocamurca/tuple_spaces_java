package br.edu.ifce.space;
import net.jini.core.entry.Entry;

import java.util.ArrayList;

public class Environment implements Entry{

    public String name;
    public ArrayList<String> users;
    public ArrayList<String> devices;
}

