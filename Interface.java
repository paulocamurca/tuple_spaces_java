package br.edu.ifce.space;
import net.jini.core.entry.Entry;
import net.jini.core.entry.UnusableEntryException;
import net.jini.core.transaction.TransactionException;
import net.jini.space.JavaSpace;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.EventListener;

import static net.jini.core.lease.Lease.FOREVER;

public class Interface {

    private ArrayList<Environment> add_again;
    public boolean always;

    public void ListEnvironment(JavaSpace space) throws TransactionException, UnusableEntryException,
            RemoteException, InterruptedException {

        add_again = new ArrayList<>();
        always = true;
        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }
        while (always) {

            Environment template = new Environment();
            Environment obj = (Environment) space.take(template, null, 100);
            add_again.add(obj);

            if (obj == null) {
                for (Environment ob : add_again) {
                    if (ob == null){
                        always = false;
                    }else {
                    space.write(ob,null, FOREVER);
                    }
                }

            }else {
                System.out.println(obj.name);
            }

        }
    }

    public void CreateEnvironment(JavaSpace space, Environment env){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }
        try {
            Environment obj = (Environment) space.take(env, null, 100);

            if (obj == null){
                space.write(env, null, FOREVER);

            }else if (obj.name.equals(env.name)){
                space.write(obj, null, FOREVER);
                System.out.println("Ambiente existe!");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void RemoveEnvironment(JavaSpace space, Environment env){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }
        try {
            Environment obj = (Environment) space.take(env, null, 100);

            if (obj != null){
                if (obj.users == null && obj.devices == null){
                    System.out.println("Ambiente removido!");

                }else {
                    space.write(obj, null, FOREVER);
                    System.out.println("Ambiente não está vazio e não pode ser removido!");
                }
//                if (obj.users.isEmpty() || obj.devices.isEmpty()){
//                    System.out.println("O ambiente está vazio!");
//
//                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void AddDevice(JavaSpace space, Environment env, String device_name){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {

            Environment obj = (Environment) space.take(env, null, 100);
            env.devices = new ArrayList<String>();

            if (obj == null){
                env.devices.add(device_name);
                space.write(env, null, FOREVER);

            }else {
                if (obj.devices != null){
                    for (int i=0; i < obj.devices.size(); i++){
                        if (obj.devices.get(i).equals(device_name)){
                            obj.devices.remove(device_name);
                            System.out.println("Dispositivo já existe");
                        }
                    }
                    obj.devices.add(device_name);
                }else {
                    obj.devices = new ArrayList<String>();
                    obj.devices.add(device_name);
                }
                space.write(obj, null, FOREVER);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void ListDevices(JavaSpace space, Environment env){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {
            Environment obj = (Environment) space.take(env, null, 100);

            if (obj == null){

                System.out.println("Ambiente não encontrado!");

            }else {
                if (obj.devices != null){
                    for (int i=0; i < obj.devices.size(); i++){
                        System.out.println(obj.devices.get(i));
                    }
                }
                space.write(obj, null, FOREVER);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void MoveDevice(JavaSpace space, Environment env1, String device_name, Environment env2){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {

            Environment obj1 = (Environment) space.take(env1, null, 100);
            Environment obj2 = (Environment) space.take(env2, null, 100);

            if (obj1 == null){
                System.out.println("Ambiente não encontrado!");

            }else {
                if (obj1.devices != null){
                    for (int i=0; i < obj1.devices.size(); i++){
                        if (obj1.devices.get(i).equals(device_name)){
                            obj1.devices.remove(device_name);
                        }
                    }
                    space.write(obj1, null, FOREVER);

                }else {
                    System.out.println("Não há dispositivos para mover");
                }
            }

            if (obj2 != null){
                if (obj2.devices != null){
                    for (int i=0; i < obj2.devices.size(); i++){
                        if (obj2.devices.get(i).equals(device_name)){
                            obj2.devices.remove(device_name);
                        }
                    }
                    obj2.devices.add(device_name);
                    space.write(obj2, null, FOREVER);
                    System.out.println("dispositivo movido!");

                }else {
                    obj2.devices = new ArrayList<String>();
                    obj2.devices.add(device_name);
                    space.write(obj2, null, FOREVER);
                    System.out.println("dispositivo movido!");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void ListUsers(JavaSpace space, Environment env){

        if (space == null) {
            System.out.println("O servico JavaSpace nao foi encontrado. Encerrando...");
            System.exit(-1);
        }

        try {
            Environment obj = (Environment) space.take(env, null, 100);

            if (obj == null){

                System.out.println("Ambiente não encontrado!");

            }else {
                if (obj.users != null){
                    for (int i=0; i < obj.users.size(); i++){
                        System.out.println(obj.users.get(i));
                    }
                } else {
                    System.out.println("Não há usuários neste ambiente!");
                }
                space.write(obj, null, FOREVER);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
